const person = {
  name: "John",
  age: 32,
  city: "San Francisco",
}

const { name, age, city } = person


const movies = [
  {
    id: 1,
    title: "A",
    description: "AA",
    duration: 120,
  }, 
  {
    id: 2,
    title: "B",
    description: "BB",
    duration: 220,
  }, 
  {
    id: 3,
    title: "C",
    description: "CC",
    duration: 60,
  }, 
  {
    id: 4,
    title: "D",
    description: "DD",
    duration: 100,
  }, 
]


const [{ id, title, description }] = movies

movies.map(movie => {
  console.log(movie.description)
})