const movies = [
  {
    id: 1,
    title: "A",
    description: "AA",
    duration: 120,
  }, 
  {
    id: 2,
    title: "B",
    description: "BB",
    duration: 220,
  }, 
  {
    id: 3,
    title: "C",
    description: "CC",
    duration: 60,
  }, 
  {
    id: 4,
    title: "D",
    description: "DD",
    duration: 100,
  }, 
]

module.exports = movies;