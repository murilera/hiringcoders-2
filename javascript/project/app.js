const movies = require('./database')
const readline = require('readline-sync')

const entry = readline.question("Search a movie? Y/N")

if (entry.toLocaleUpperCase() === "Y") {
  const output = movies.map(movie => movie)

  console.table(output)
} else {
  console.log("Thanks, Bye!")
}