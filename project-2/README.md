# Desafio 2# - Gestão de Cadastros

## Entregáveis:

- Os dados de clientes, endereços e produtos devem estar devidamente estruturados
- Dados no localstorage
- Código fonte no github com o arquivo README detalhando as funcionalidades da programação

## Entregues:

- Código React com Lógica interável para construção da vitrine.
- Implementação de material-ui no desenvolvimento.
- Lógica de LocalStorage: Faltou tempo de salvar/atualizar o cadastro de cliente e produto na localstorage. (Implementado 26/07 00:50:50)
- Iterar sobre conteúdo da LocalStorage para montar a vitrine. (Implementado 26/07 00:50:50)

## Dificuldades:

- Falta de tempo para assistir todas as aulas e conseguir implementar os conhecimentos.

## Desconhecido:

- Aparentemente, com o deploy no netlify, as rotas quebraram... (Fixed 26/07 00:55:44)

## Novo Link:

- [Link para página atualizada](https://gallant-swirles-e14bbc.netlify.app/)
