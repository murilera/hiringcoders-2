import React, { useEffect, useState } from "react";
import UpperBar from '../../components/upperBar';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function Clientes(){
  const [title, setTitle] = useState("")
  const [description, setDescription] = useState("")
  const [price, setPrice] = useState("")
  const [image, setImage] = useState("")

  const cadastroCliente = [
    {
      text: 'Titulo',
      id: 'title',
      className: 'titleClass',
      value: title,
      set: setTitle
    },
    {
      text: 'Descrição do Produto',
      id: 'description',
      className: 'descritionClass',
      value: description,
      set: setDescription
    },
    {
      text: 'Preço',
      id: 'price',
      className: 'priceClass',
      value: price,
      set: setPrice,
    },
    {
      text: 'Url Imagem',
      id: 'image_url',
      className: 'imageurlClass',
      value: image,
      set: setImage
    },
  ]

  const handleCadastro = () => {
    const listaProdutosAtual = JSON.parse(localStorage.getItem('listaProdutosAtual'))
    const listaProdutoNovo = {
      title: title,
      description: description,
      price: price,
      image: image
    }

    if (listaProdutosAtual) {
      const listaProdutosAtualizada = listaProdutosAtual.concat(listaProdutoNovo)
      console.log(listaProdutosAtualizada)
      localStorage.setItem('listaProdutosAtual', JSON.stringify(listaProdutosAtualizada))
    } else {
      console.log(listaProdutoNovo)
      localStorage.setItem('listaProdutosAtual', JSON.stringify([listaProdutoNovo]))
    }

    setTitle("")
    setDescription("")
    setPrice("")
    setImage("")
    toast.success("Cadastrado!");
  }


  return (
    <div id="upper">
    <UpperBar/>

    <Box component="span" m={2}>
      <div class="upperContent">
        Cadastro de Produtos
      </div>
    </Box>
    
    <Box component="span" m={2}>
      <div class="upperContent">
        <form>

          <form >
            {cadastroCliente.map(item => {
              return (
                <div class={item.className}>
                  <label for={item.id}> {item.text}: </label>
                  <input type="text" name={item.id} id={item.id} value={item.value} onChange={e => {item.set(e.target.value)}}/>
                </div>
              )
            })}
          </form>
          
        </form>
      </div>
    </Box>
  
    <div>
      <Button variant="contained" size="small" color="primary" onClick={handleCadastro}>
        Cadastrar!
      </Button>
    </div>
    <ToastContainer
      position="bottom-center"
      autoClose={5000}
      hideProgressBar={false}
      newestOnTop={false}
      closeOnClick
      rtl={false}
      pauseOnFocusLoss
      draggable
      pauseOnHover
      />
    
  </div>
  )

}

export default Clientes;