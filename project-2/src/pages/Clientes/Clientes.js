import React, { useEffect, useState } from "react";
import UpperBar from '../../components/upperBar';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function Clientes(){
  const [nome, setNome] = useState("")
  const [cpf, setCpf] = useState("")
  const [endereco, setEndereco] = useState("")
  const [phone, setPhone] = useState("")
  const [email, setEmail] = useState("")

  const cadastroCliente = [
    {
      text: 'Nome',
      id: 'name',
      className: 'nameClass',
      value: nome,
      set: setNome
    },
    {
      text: 'CPF',
      id: 'cpf',
      className: 'cpfClass',
      value: cpf,
      set: setCpf
    },
    {
      text: 'Endereço',
      id: 'address',
      className: 'addressClass',
      value: endereco,
      set: setEndereco
    },
    {
      text: 'Telefone',
      id: 'phone',
      className: 'phoneClass',
      value: phone,
      set: setPhone
    },
    {
      text: 'Email',
      id: 'email',
      className: 'emailClass',
      value: email,
      set: setEmail
    },

  ]

  const handleCadastro = () => {
    const listaCadastroAtual = localStorage.getItem('listaCadastroAtual')
    const listaCadastroNovo = {
      nome: nome,
      cpf: cpf,
      endereco: endereco,
      phone: phone,
      email: email
    }

    if (listaCadastroAtual) {
      const listaCadastroAtualizada = listaCadastroAtual.concat(listaCadastroNovo)
      console.log(listaCadastroAtualizada)
      localStorage.setItem('listaCadastroAtual', JSON.stringify(listaCadastroAtualizada))
    } else {
      console.log(listaCadastroNovo)
      localStorage.setItem('listaCadastroAtual', JSON.stringify([listaCadastroNovo]))
    }

    setNome("")
    setCpf("")
    setEndereco("")
    setPhone("")
    setEmail("")
    toast.success("Cadastrado!");
  }


  return (
    <div id="upper">
      <UpperBar/>

      <Box component="span" m={2}>
        <div class="upperContent">
          Cadastro de Clientes
        </div>
      </Box>
      
      <Box component="span" m={2}>
        <div class="upperContent">
          <form>

            <form>
              {cadastroCliente.map(item => {
                return (
                  <div class={item.className}>
                    <label for={item.id}> {item.text}: </label>
                    <input type="text" name={item.id} id={item.id} value={item.value} onChange={e => {item.set(e.target.value)}}/>
                  </div>
                )
              })}
            </form>
            

          </form>
        </div>
      </Box>
    
      <div>
        <Button variant="contained" size="small" color="primary" onClick={handleCadastro} >
          Cadastrar!
        </Button>
      </div>
      <ToastContainer
      position="bottom-center"
      autoClose={5000}
      hideProgressBar={false}
      newestOnTop={false}
      closeOnClick
      rtl={false}
      pauseOnFocusLoss
      draggable
      pauseOnHover
      />
    </div>
  )

}

export default Clientes;