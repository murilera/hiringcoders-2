import { React, useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import UpperBar from '../../components/upperBar';
import Card from '../../components/card';
import { Grid } from "@material-ui/core";


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  link: {
    padding: theme.spacing(2),
  },
  gridContainer: {
    paddingLeft: "40px",
    paddingRight: "40px"
  }
}));

export default function Home() {
  const classes = useStyles();
  const listaProdutosAtual = localStorage.getItem('listaProdutosAtual')

  var p = [{
    title: "Teste",
    image_url: "https://static.netshoes.com.br/produtos/tenis-adidas-breaknet-feminino/28/NQQ-4379-028/NQQ-4379-028_zoom1.jpg?ts=1602007269",
    description: "aaaa",
    price: "11"
  },
  {
    title: "Teste2",
    image_url: "https://static.netshoes.com.br/produtos/tenis-de-caminhada-leve-confortavel/06/E74-0492-006/E74-0492-006_zoom1.jpg?ts=1603739403&ims=544x",
    description: "bbbb",
    price: "12"
  },
  {
    title: "Teste3",
    image_url: "https://static.dafiti.com.br/p/Evoltenn-T%C3%AAnis-Evoltenn-Easy-Style-Preto-Amarelo-1382-3414617-1-zoom.jpg",
    description: "cccc",
    price: "13"
  },
  {
    title: "Teste4",
    image_url: "https://a-static.mlcdn.com.br/618x463/tenis-academia-feminino-run/bfshoes/a001f-preto-36/3deb58ac785b2a1cb9b6df89bfa9eed9.jpg",
    description: "dddd",
    price: "14"
  }
  ]

  if (listaProdutosAtual){
    p = JSON.parse(listaProdutosAtual)
  }

  useEffect(() => { 
    window.addEventListener('storage', () => {   
    });
  }, [])
  

  return (
    <div>
      <div style={{'paddingTop': '1px'}}>
        <UpperBar/>
      </div>

      <div style={{'paddingTop': '20px'}}>
        
        <Grid
          container
          spacing={1}
          className={classes.gridContainer}
          justify="flex"
        >
        {p.map(item => {
          return (
            <Grid item xs={2}>
              <Card products={item} />
            </Grid>
          )
        })}

        </Grid>
      </div>
    </div>
  );
}
