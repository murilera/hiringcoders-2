import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Home from './pages/Home/Home'
import Clientes from './pages/Clientes/Clientes'
import Produtos from './pages/Produtos/Produtos'


function Routes(){
  return(
    <BrowserRouter>
      <Switch>
        {<Route path="/" exact component={() => <Home />}/>}
        {<Route path="/clientes" exact component={() => <Clientes />}/>}
        {<Route path="/produtos" exact component={() => <Produtos />}/>}
      </Switch>
    </BrowserRouter>
  )
}

export default Routes;