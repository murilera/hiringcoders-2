import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  link: {
    padding: theme.spacing(2),
  }
}));

const UpperBar = () => {
  const classes = useStyles();
  const pages = [
    {
      text: "Home",
      link: "/"
    },{
      text: "Clientes",
      link: "/clientes"
    },
    {
      text: "Produtos",
      link: "/produtos"
    },
  ]

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            {pages.map(item => {
              return (
                <Link href={item.link} color='inherit' className={classes.link}>
                  {item.text}
                </Link>
              )
            })}
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
}

export default UpperBar;