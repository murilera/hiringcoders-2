# HiringCoders-2

## [HC] Módulo 01 - Introdução Introdução

## [HC] Módulo 02 - Lógica de Programação Lógica de Programação

## [HC] Módulo 03 - GIT

## [HC] Extra - HTML

## [HC] Extra - CSS

## [HC] Extra - Estrutura de Dados

## [HC] Extra - Metodologia Ágeis

## [HC] Extra - Boas Práticas

## [HC] Módulo 04 - JavaScript Básico

## [HC] Módulo 05 - JavaScript II

## [HC] Módulo 06 - Typescript

## [HC] Módulo 07 - React

## [HC] Módulo 08 - GraphQL

## [HC] Módulo 09 - Node.JS Parte I

## [HC] Módulo 10 - Node.JS Parte II
